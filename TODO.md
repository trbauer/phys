# FEATURES
* Embed the world constants into the shader as preprocessor tokens so they track CPU.
* Check for particle quiescence at rest.
* Render shader to draw particle debug colors
* set particle state as bitset of one of the floats
  uint32_t attrs;
  const uint32_t ATTR_DEAD   = (1u << 0)

* Improve lighting
* Add git submodules to get dependencies
* Add animated gif of it running



# BUGS
* Jitter of particles at rest
* "Strainer" effect when using GPU physics (repro with CPU physics?)
* Particle count bug (see below)



## Particle count bug
  increasing particles to 524288
  F5: redrawing
  gpu physics: enabled
  increasing particles to 1048576
  render VBO: enabled
  gpu physics: disabled
  gpu physics: enabled
  increasing particles to 2097152
  0x71
  mouseClick: button:2, state:0
  mouseClick: button:2, state:1
  decreasing particles to 100000