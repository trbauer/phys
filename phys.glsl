#version 430 compatibility
// #extension GL_ARB_compute_shader : enable;
// #extension GL_ARB_shader_storage_buffer_object : enable;

layout (local_size_x = 32) in;

uniform float dt;
uniform float W;

struct particle
{
    vec3 pos; float _padding1;
    vec3 vel; float _padding2;

    vec4 pos0; vec4 vel0;
};

const float MAX_VEL = 32.0f;
const float DAMPING_FLOOR = 0.2f;
const float DAMPING_SPHERE = 0.8f;
const int SPHERE_COUNT = ${SPHERE_COUNT};
uniform	vec3 spherePos[SPHERE_COUNT];
uniform	float sphereRad[SPHERE_COUNT];

layout (std430, binding=2) buffer particles
{
    particle ps[];
};

void main()
{
    // TODO: check for quiescence

    particle p = ps[gl_GlobalInvocationID.x];
    vec3 pos = p.pos;
    vec3 vel = p.vel;

    vel.y += -9.8f * dt;
    vel.y = clamp(vel.y,-MAX_VEL,MAX_VEL);
    pos += dt * vel;

    if (pos.y < -W) {
        pos.y = -W;
        vel.y = -vel.y;
        vel *= DAMPING_FLOOR;
    }

    if (pos.x < -W) {
        pos.x = -W;
        vel.x = -vel.x;
        vel *= DAMPING_FLOOR;
    }
    if (pos.x > W) {
        pos.x = W;
        vel.x = -vel.x;
        vel *= DAMPING_FLOOR;
    }

    if (pos.z < -W) {
        pos.z = -W;
        vel.z = -vel.z;
        vel *= DAMPING_FLOOR;
    }
    if (pos.z > W) {
        pos.z = W;
        vel.z = -vel.z;
        vel *= DAMPING_FLOOR;
    }

    for (int i = 0; i < SPHERE_COUNT; i++) {
        vec3 sp = spherePos[i];
	      float rad = sphereRad[i];
	      vec3 n = pos - sp;
	      if (dot(n,n) <= rad * rad) {
	          vel = DAMPING_SPHERE * reflect(vel,normalize(n));
		        pos = sp + n;
	          break;
	      }
    }
    // TODO: check for quiescence
    // ps[gl_GlobalInvocationID.x] = p;
    ps[gl_GlobalInvocationID.x].pos = pos;
    ps[gl_GlobalInvocationID.x].vel = vel;
}