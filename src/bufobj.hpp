#ifndef BUFOBJ_HPP
#define BUFOBJ_HPP

#include "gl.hpp"

#include <functional>
#include <cstdint>
#include <iostream>

template<typename T> using ReadElemFunction =
    std::function<void(const T &elem)>;
template<typename T> using ReadElemIxFunction =
    std::function<void(const T &elem, size_t index)>;
template<typename T> using ReadBufferFunction =
    std::function<void(const T *buf, size_t len)>;
//
template<typename T> using ModifyElemFunction =
    std::function<void(T &elem)>;
template<typename T> using ModifyElemIxFunction =
    std::function<void(T &elem, size_t index)>;
template<typename T> using ModifyBufferFunction =
    std::function<void(T *buf, size_t len)>;

template <typename T>
struct buffer_map {
    T *ptr;
    GLuint buffer_name;
    GLenum buffer_type;
    buffer_map(GLenum _buffer_type, GLuint _buffer_name, GLenum access)
        : buffer_type(_buffer_type)
        , buffer_name(_buffer_name)
    {
        GL_CALL(glBindBuffer, buffer_type, buffer_name);
        ptr = (T *)glMapBuffer(buffer_type, access);
        checkGlErrors("glMapBuffer");
        GL_CALL(glBindBuffer, buffer_type, 0);
//        std::cout << "buffer_map(0x" << std::hex << std::uppercase << (uint64_t)ptr << "): created\n";
    }
//
//    buffer_map(buffer_map &&m) {
//        std::cout << "buffer_map(0x" << std::hex << std::uppercase << (uint64_t)ptr << "): rvalue move\n";
//        ptr = m.ptr; // FIXME: zero buffer_name; disable destructor calls
//    }
    ~buffer_map() {
//        std::cout << "buffer_map(0x" << std::hex << std::uppercase << (uint64_t)ptr << ") destroying\n";
        ptr = nullptr;
        // GL_CALL(glUnmapNamedBuffer, buffer_name);
        GL_CALL(glBindBuffer, buffer_type, buffer_name);
        GL_CALL(glUnmapBuffer, buffer_type);
        GL_CALL(glBindBuffer, buffer_type, 0);
    }
};

// SSBO: http://www.geeks3d.com/20140704/tutorial-introduction-to-opengl-4-3-shader-storage-buffers-objects-ssbo-demo/
// ALSO:
//   http://antongerdelan.net/opengl/compute.html
//   http://wili.cc/blog/entries/opengl-cs/opengl_cs.cpp
//   http://wili.cc/blog/opengl-cs.html
//   http://blogs.evergreen.edu/vistas/files/2013/03/bailey-g3vis-proof4.pdf
//   https://www.cg.tuwien.ac.at/courses/Realtime/repetitorium/rtr_rep_2016_ComputeShader
template<typename T>
struct bufobj
{
    // e.g. bufobj(GL_SHADER_STORAGE_BUFFER, 128*sizeof(T))
    bufobj(GLenum _buffer_type, size_t num_elems, GLenum usage)
        : buffer_name(0)
        , buffer_type(_buffer_type)
        , length(num_elems)
    {
        GL_CALL(glGenBuffers, 1, &buffer_name);
        GL_CALL(glBindBuffer, buffer_type, buffer_name);
        // difference between glBufferData and glBufferStorage is in the immutability
        // I think I need glMapSubBuffer or something for glBufferStorage?

        // The frequency of access may be one of these:
        //   STREAM  The data store contents will be modified once and used at most a few times.
        //   STATIC  The data store contents will be modified once and used many times.
        //   DYNAMIC The data store contents will be modified repeatedly and used many times.
        //
        // The nature of access may be one of these:
        //   DRAW   The data store contents are modified by the application, and
        //          used as the source for GL drawing and image specification commands.
        //   READ   The data store contents are modified by reading data from the GL,
        //          and used to return that data when queried by the application.
        //   COPY   The data store contents are modified by reading data from the GL,
        //          and used as the source for GL drawing and image specification commands.
        GL_CALL(glBufferData,
            buffer_type,
            num_elems * sizeof(T),
            nullptr,
            usage);
        //    GL_CALL(glBufferStorage,
        //        buffer_type,
        //        num_bytes,
        //        nullptr,
        //        GL_DYNAMIC_STORAGE_BIT|GL_MAP_READ_BIT|GL_MAP_WRITE_BIT);
        GL_CALL(glBindBuffer, buffer_type, 0);
    }

    ~bufobj() {
        GL_CALL(glDeleteBuffers, 1, &buffer_name);
    }

    void read(ReadElemIxFunction<T> func) const {
        buffer_map<T> m = map(GL_READ_ONLY);
        for (size_t i = 0; i < length; i++)
            func(m.ptr[i], i);
    }
    void modify(ModifyElemIxFunction<T> func) {
        buffer_map<T> m = map(GL_READ_WRITE);
        for (size_t i = 0; i < length; i++)
            func(m.ptr[i], i);
    }
    void read(ReadElemFunction<T> func) const {
        buffer_map<T> m = map(GL_READ_ONLY);
        for (size_t i = 0; i < length; i++)
            func(m.ptr[i]);
    }
    void modify(ModifyElemFunction<T> func) {
        buffer_map<T> m = map(GL_READ_WRITE);
        for (size_t i = 0; i < length; i++)
            func(m.ptr[i]);
    }
    void read(ReadBufferFunction<T> func) const {
        buffer_map<T> m = map(GL_READ_ONLY);
        func(m.ptr, size());
    }
    void modify(ModifyBufferFunction<T> func) {
        buffer_map<T> m = map(GL_READ_WRITE);
        func(m.ptr, size());
    }
    buffer_map<T> map(GLenum attrs) const {
        return buffer_map<T>(buffer_type, buffer_name, attrs);
    }

    size_t size() const {return length;}
    GLuint bufferId() const {return buffer_name;}
private:
    GLuint buffer_name;
    GLenum buffer_type;
    size_t length;
};


#endif