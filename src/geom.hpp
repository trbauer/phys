#ifndef GEOM_HPP
#define GEOM_HPP

#include "glm/glm.hpp"
#define GLM_ENABLE_EXPERIMENTAL
#include "glm/gtx/norm.hpp" // glm::length2/distance2

// #include <cmath>


/*
template <typename T>
static T clamp(T lo, T hi, T val) {
return val < lo ? lo : val > hi ? hi : val;
}

struct vec3 {
    union {
        float arr[4];
        struct {float x, y, z;};
    };

    vec3() : vec3(0,0,0) { }
    vec3(float x, float y, float z) { arr[0] = x; arr[1] = y; arr[2] = z; }

    float len2() const { return x*x + y*y + z*z; }
    float len() const { return sqrtf(len2()); }
    vec3 norm() const { return scaled(1.0f/len()); }
    vec3 scaled(float s) const { return vec3(s*x, s*y, s*z); }

    void operator+=(vec3 arg) {x += arg.x; y += arg.y; z += arg.z;}
    void operator-=(vec3 arg) {x -= arg.x; y -= arg.y; z -= arg.z;}
    void operator*=(float s) {x *= s; y *=s; z*=s; }
};

static vec3 operator+(vec3 a, vec3 b) {return vec3(a.x + b.x, a.y + b.y, a.z + b.z);}
static vec3 operator-(vec3 a, vec3 b) {return vec3(a.x - b.x, a.y - b.y, a.z - b.z);}
static vec3 operator*(float s, vec3 a) {return a.scaled(s);}
static vec3 operator*(vec3 a,float s) {return s * a;}
static float dot(vec3 a, vec3 b) {return a.x*b.x + a.y*b.y + a.z*b.z;}

template <int W,int H>
struct mat {
    float arr[W*H];

    void clear() {for (int i = 0; i < W*H; i++) arr[i] = 0.0f;}
    void operator*=(mat<W,H> m) {*this = *this * m;}
    void operator*=(float s) {for (int i = 0; i < W*H; i++) arr[i] *= s;}
    void operator+=(mat<W,H> m) {for (int i = 0; i < W*H; i++) arr[i] += m[i];}
    void operator-=(mat<W,H> m) {for (int i = 0; i < W*H; i++) arr[i] -= m[i];}
    float* operator[](int row) {return arr + W*row;}
    const float* operator[](int row) const {return arr + W*row;}
};


template <int W,int H>
static mat<W,H> operator+(mat<W,H> m0, mat<W,H> m1) {
    mat<W,H> m;
    for (int r = 0; r < H; r++) {
        for (int c = 0; c < W; c++) {
            m[r][c] = m0[r][c] + m1[r][c];
        }
    }
    return m;
}
template <int W,int H>
static mat<W,H> operator-(mat<W,H> m0, mat<W,H> m1) {
    mat<W,H> m;
    for (int r = 0; r < H; r++) {
        for (int c = 0; c < W; c++) {
            m[r][c] = m0[r][c] - m1[r][c];
        }
    }
    return m;
}
template <int W,int H>
static mat<W,H> operator*(mat<W,H> m0, mat<W,H> m1) {
    mat<W,H> m;
    for (int r = 0; r < H; r++) {
        for (int c = 0; c < W; c++) {
            float dp = 0.0f;
            for (int i = 0; i < H; i++) {
                dp += m0[r][i]*m1[i][c];
            }
            m[r][c] = dp;
        }
    }
    return m;
}

typedef mat<3,3> mat3;
*/

#endif