#include "gl.hpp"

#include <iomanip>
#include <iostream>
#include <string>
#include <sstream>
#include <Windows.h>

#define CASES(X,Y) case X: ss << (Y); break
#define CASE(X) CASES(X,#X)

static std::string debugSource(GLenum val)
{
    std::stringstream ss;

    switch (val) {
    CASE(GL_DEBUG_SOURCE_API);
    CASE(GL_DEBUG_SOURCE_WINDOW_SYSTEM);
    CASE(GL_DEBUG_SOURCE_SHADER_COMPILER);
    CASE(GL_DEBUG_SOURCE_APPLICATION);
    CASE(GL_DEBUG_SOURCE_THIRD_PARTY);
    CASE(GL_DEBUG_SOURCE_OTHER);
    default: ss << "0x" << std::hex << std::setfill('0') << std::setw(4) << val;
    }
    return ss.str();
}

static std::string debugType(GLenum val)
{
    std::stringstream ss;
    switch (val) {
    CASE(GL_DEBUG_TYPE_ERROR);
    CASE(GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR);
    CASE(GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR);
    CASE(GL_DEBUG_TYPE_PORTABILITY);
    CASE(GL_DEBUG_TYPE_PERFORMANCE);
    CASE(GL_DEBUG_TYPE_MARKER);
    CASE(GL_DEBUG_TYPE_PUSH_GROUP);
    CASE(GL_DEBUG_TYPE_POP_GROUP);
    CASE(GL_DEBUG_TYPE_OTHER);
    default: ss << "0x" << std::hex << std::setfill('0') << std::setw(4) << val << "?";
    }
    return ss.str();
}

static std::string debugSeverity(GLenum val)
{
    std::stringstream ss;
    switch (val) {
    CASES(GL_DEBUG_SEVERITY_HIGH,"HIGH");
    CASES(GL_DEBUG_SEVERITY_MEDIUM,"MED.");
    CASES(GL_DEBUG_SEVERITY_LOW,"LOW");
    CASES(GL_DEBUG_SEVERITY_NOTIFICATION,"NOTE");
    default: ss << "0x" << std::hex << std::setfill('0') << std::setw(4) << val << "?";
    }
    return ss.str();
}

#undef CASE


void APIENTRY openglDebugCallback(
    GLenum source,
    GLenum type,
    GLuint id,
    GLenum severity,
    GLsizei length,
    const GLchar *message,
    const void *userParam)
{
    // https://www.khronos.org/opengl/wiki/GLAPI/glDebugMessageControl

    // source =
    //   GL_DEBUG_SOURCE_API                    0x8246
    //   GL_DEBUG_SOURCE_WINDOW_SYSTEM          0x8247?
    //   GL_DEBUG_SOURCE_SHADER_COMPILER        0x8248?
    //   GL_DEBUG_SOURCE_APPLICATION            0x824A
    //   GL_DEBUG_SOURCE_THIRD_PARTY            0x8249
    //   GL_DEBUG_SOURCE_OTHER                  0x824B
    // type =
    //   GL_DEBUG_TYPE_ERROR                    0x824C
    //   GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR      0x824D
    //   GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR       0x824E
    //   GL_DEBUG_TYPE_PORTABILITY              0x824F
    //   GL_DEBUG_TYPE_PERFORMANCE              0x8250
    //   GL_DEBUG_TYPE_MARKER                   0x8268
    //   GL_DEBUG_TYPE_PUSH_GROUP               0x8269
    //   GL_DEBUG_TYPE_POP_GROUP                0x826A
    //   GL_DEBUG_TYPE_OTHER                    0x8251
    // id = 0x20071
    // severity =
    //   GL_DEBUG_SEVERITY_HIGH                 0x9146
    //   GL_DEBUG_SEVERITY_MEDIUM               0x9147
    //   GL_DEBUG_SEVERITY_LOW                  0x9148
    //   GL_DEBUG_SEVERITY_NOTIFICATION         0x826B
    std::cerr << "OpenGL Message[" << debugSeverity(severity) << "]: " <<
        message << "\n";
    if (severity != GL_DEBUG_SEVERITY_NOTIFICATION) {
        if (IsDebuggerPresent()) {
            DebugBreak();
        }
    }
}


void drawAxes(int units)
{
    glBegin(GL_LINES);
    for (int u = 0; u < units; u++) {
        glColor3f(u % 2 ? 0.5f : 1.0f,0,0);
        glVertex3f((float)u,0,0);
        glVertex3f((float)(u+1),0,0);
    }

    for (int u = 0; u < units; u++) {
        glColor3f(0.0f,u % 2 ? 0.5f : 1.0f,0);
        glVertex3f(0,(float)u,0);
        glVertex3f(0,(float)(u+1),0);
    }

    for (int u = 0; u < units; u++) {
        glColor3f(0,0,u % 2 ? 0.5f : 1.0f);
        glVertex3f(0,0,(float)u);
        glVertex3f(0,0,(float)(u+1));
    }
    glEnd();
}

std::string matrixString(GLenum which)
{
    std::stringstream ss;
    GLint d;
    if (which == GL_MODELVIEW_MATRIX) {
        glGetIntegerv(GL_MODELVIEW_STACK_DEPTH,&d);
        ss << "modelview[" << d << "]";
    } else if (which == GL_PROJECTION_MATRIX) {
        glGetIntegerv(GL_PROJECTION_STACK_DEPTH,&d);
        ss << "projection[" << d << "]";
    } else {
        ss << "unknown matrix[]";
    }
    ss << "\n";

    double m[16];
    glGetDoublev(which,m);

    auto emitRow = [&](int at) {
        ss << "|";
        ss << std::fixed;
        ss << std::setw(7) << std::setprecision(3) << m[at+0] << " "
           << std::setw(7) << std::setprecision(3) << m[at+1] << " "
           << std::setw(7) << std::setprecision(3) << m[at+2] << " "
           << std::setw(7) << std::setprecision(3) << m[at+3] << " ";
        ss << "|\n";
    };
    emitRow(0);
    emitRow(4);
    emitRow(8);
    emitRow(12);
    return ss.str();
}

// static void *font = GLUT_BITMAP_HELVETICA_12; // GLUT_BITMAP_TIMES_ROMAN_24;
static void *font = GLUT_BITMAP_9_BY_15;
void drawString(int x, int y, const char *string)
{
    int w = glutGet(GLUT_WINDOW_WIDTH);
    int h = glutGet(GLUT_WINDOW_HEIGHT);

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D(0,w,h,0);
    glDisable(GL_DEPTH_TEST);
    glRasterPos2i(x, y);
    glutBitmapString(font, (const unsigned char *)string);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    glEnable(GL_DEPTH_TEST);
    // int len = (int)strlen(string);
    // for (int i = 0; i < len; i++) {
    //     glutBitmapCharacter(font, string[i]);
    // }
}

std::string  format(glm::vec3 v)
{
    std::stringstream ss;
    ss << "<";
    ss << std::fixed << std::setprecision(4) << std::setw(7) << v.x;
    ss << ",";
    ss << std::fixed << std::setprecision(4) << std::setw(7) << v.y;
    ss << ",";
    ss << std::fixed << std::setprecision(4) << std::setw(7) << v.z;
    ss << ">";
    return ss.str();
}

void drawFormat(int x, int y, const char *fmt, ...)
{
    char buf[1024];
    va_list va;
    va_start(va,fmt);
    vsnprintf(buf,sizeof(buf),fmt,va);
    va_end(va);
    drawString(x,y,buf);
}


int64_t now()
{
    LARGE_INTEGER li;
    QueryPerformanceCounter(&li);
    return li.QuadPart;
}
double ticksToSeconds(int64_t ts)
{
    static double to_seconds;
    if (to_seconds == 0.0f) {
        LARGE_INTEGER li;
        QueryPerformanceFrequency(&li);
        to_seconds = 1.0/li.QuadPart;
    }
    return ts*to_seconds;
}


std::string formatGlError(GLint error)
{
#define CASE(X) case X: return #X;
    switch (error) {
    CASE(GL_NO_ERROR);
    CASE(GL_INVALID_ENUM);
    CASE(GL_INVALID_VALUE);
    CASE(GL_INVALID_OPERATION);
    CASE(GL_INVALID_FRAMEBUFFER_OPERATION);
    CASE(GL_OUT_OF_MEMORY);
    CASE(GL_STACK_OVERFLOW);
    CASE(GL_STACK_UNDERFLOW);
    default: return "UNKNOWN";
    }
}

void clearGlErrors()
{
    while (true) {
        GLint err = glGetError();
        if (err == GL_NO_ERROR) {
            break;
        }
    }
}
void checkGlErrors(const char *api)
{
    std::stringstream ss;
    int num_errs = 0;
    GLint err = GL_NO_ERROR;
    while ((err = glGetError()) != GL_NO_ERROR) {
        if (num_errs++ == 0) {
            ss << "=========== OpenGL errors";
            if (api) {
                ss << " after " << api;
            }
        }
        ss << "\n";
        ss << formatGlError(err) << " (" << err << ")";
    }
    if (num_errs > 0) {
        std::cerr << ss.str() << "\n";
        if (IsDebuggerPresent()) {
            DebugBreak();
        }
        exit(1);
    }
}
