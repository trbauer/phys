#ifndef GL_HPP
#define GL_HPP

#include "GL/glew.h"
// #define FREEGLUT_STATIC
#include "GL/freeglut.h"
#include "geom.hpp"

#include <string>
#include <cstdarg>
#include <cstdint>

#define GL_CALL(F,...)\
  {\
    F(__VA_ARGS__);\
    checkGlErrors(#F);\
  }


void         clearGlErrors();
void         checkGlErrors(const char *api = nullptr);
std::string  formatGlError(GLint error);

void         drawAxes(int units = 1);
void         drawString(int x, int y, const char *string);
void         drawFormat(int x, int y, const char *fmt, ...);

std::string  matrixString(GLenum which);
std::string  format(glm::vec3 v);

void APIENTRY openglDebugCallback(
    GLenum source,
    GLenum type,
    GLuint id,
    GLenum severity,
    GLsizei length,
    const GLchar *message,
    const void *userParam);


int64_t now();
double ticksToSeconds(int64_t);

template <int N,typename T>
class sampler
{
    int64_t samples[N];
    int64_t last_start;
    int next_index;
public:
    sampler() {}
    void add(T x) { samples[next_index++] = x; next_index %= N; }
    double average() const {
        T sum = 0;
        for (T s : samples) sum += s;
        return sum/(double)N;
    }
};


template<int N>
class time_sampler
{
    sampler<N,int64_t> s;
    int64_t last_start;
public:
    time_sampler() {}
    void start() { last_start = now(); }
    void end() { s.add(now() - last_start); }
    double average() const {
        return ticksToSeconds((uint64_t)s.average());
    }
};


#define SAMPLE_TIME(t) \
    scoped_timer<64> _st(t)

template<int N>
struct scoped_timer
{
    time_sampler<N> &ts;
    scoped_timer(time_sampler<N> &_ts) : ts(_ts) {
        ts.start();
    }
    ~scoped_timer() {
        ts.end();
    }
};

#endif