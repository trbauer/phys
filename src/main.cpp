#include "gl.hpp"
#include "geom.hpp"
#include "phys.hpp"

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <cmath>
#include <sstream>
#include <vector>



static sim *pSim;

// static GLfloat x_rot = -25.0f, y_rot = 25.0f;
static GLfloat x_rot = 20.0f, y_rot = 35.0f;
static GLfloat scale = 1.0f;
static size_t num_particles = 32; // 1;
static int num_spheres = 4;
static size_t seed = 2024;
static bool animate = false;

static double last_t_s;
static double time_dialation = 1.0f;

static time_sampler<64> times_physics;
static time_sampler<64> times_render;
static time_sampler<64> times_other;

int verbosity = 0;

// static sampler<64,double> dt_values;

////////////////////////////////
// Counters ProfileCall(std::function<void ()> func)
// {
// https://www.khronos.org/registry/OpenGL/extensions/ARB/ARB_pipeline_statistics_query.txt
// https://books.google.com/books?id=Nwo0CgAAQBAJ&pg=PT786&lpg=PT786&dq=glBeginQueryIndexed&source=bl&ots=LpNSWiFso1&sig=rJlPrdi5ycg6QbS0vL2q2BXSqh8&hl=en&sa=X&ved=0ahUKEwiLlZ7b9LnWAhXhxFQKHc3RDxIQ6AEITTAH#v=onepage&q=glBeginQueryIndexed&f=false
// glBeginQuery/glEndQuery https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glBeginQuery.xhtml
// glBeginQueryIndexed https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glBeginQueryIndexed.xhtml
// glGetQueryObject https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glGetQueryObject.xhtml
// glGenQueries https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glGenQueries.xhtml
//
// GL_VERTICES_SUBMITTED
// PRIMITIVES_SUBMITTED
// COMPUTE_SHADER_INVOCATIONS
// VERTEX_SHADER_INVOCATIONS
// FRAGMENT_SHADER_INVOCATIONS
// }

static void display(void)
{
    checkGlErrors();
    times_other.end();
    {
        SAMPLE_TIME(times_render);
        // std::cout << "display: " << x_rot << ", " << y_rot << "\n";
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        gluLookAt(
            0,  0, 15, // pos
            0,  0,  0, // looking at
            0,  1,  0);
        glRotatef(x_rot, 1, 0, 0);
        glRotatef(y_rot, 0, 1, 0);
        glScalef(scale, scale, scale);

        drawAxes((int)(WORLD_SIDE + 0.5f));
        if (pSim) {
            pSim->render();
        }

        glColor3f(1,1,1);
        // drawString(  0,  24, "(  0, 24) This is written in a GLUT bitmap font.");
        // drawString(100, 100, "(100,100) ABCDEF");
        // drawString( 50, 145, "( 50,145) positioned in pixels with upper-left origin");
        drawFormat(5,20,"physics    %5.3f ms (%s) (use 'p')",
            times_physics.average()*1000.0f,
            pSim && pSim->isUsingGpuPhysics() ? "GPU" : "CPU");
        drawFormat(5,40,"draw       %5.3f ms (%s) (use 'g')",
            times_render.average()*1000.0f,
            pSim && pSim->isUsingVBO() ? "GPU" : "CPU");
        drawFormat(5,60,"other      %5.3f ms",
            times_other.average()*1000.0f);
        drawFormat(5,80,"particles: %d  ([ and ] to halve/double)",
            num_particles);
        if (time_dialation > 1.0f) {
            drawFormat(5,100,"time dialated %5.1f %%",
                100.0f*time_dialation);
        } else {
            drawFormat(5,100,"time: real-time");
        }

        glutSwapBuffers();
    }
    times_other.start();
}

static void advect(float dt_s)
{
    if (pSim) {
        times_other.end();
        {
            SAMPLE_TIME(times_physics);
            pSim->advect((float)dt_s);
        }
        times_other.start();
    }

    glutPostRedisplay();
}

static void idle(void)
{
    double t_s = glutGet(GLUT_ELAPSED_TIME)/1000.0;
    double dt_s = t_s - last_t_s;
    if (dt_s > MAX_DT) {
        time_dialation = dt_s/MAX_DT;
        dt_s = MAX_DT;
    } else {
        time_dialation = 1.0f;
    }
    last_t_s = t_s;

    advect((float)dt_s);

    glutPostRedisplay();
}

static void visible(int vis)
{
    if (vis == GLUT_VISIBLE && animate)
        glutIdleFunc(idle);
    else
        glutIdleFunc(nullptr);
}

static void special(int chr, int x, int y)
{
    switch (chr) {
    case GLUT_KEY_LEFT:   y_rot += 5.0f; break;
    case GLUT_KEY_RIGHT:  y_rot -= 5.0f; break;
    case GLUT_KEY_UP:     x_rot += 5.0f; break;
    case GLUT_KEY_DOWN:   x_rot -= 5.0f; break;
    case GLUT_KEY_F5:
        if (!animate)
            std::cout << "F5: redrawing\n";
        break;
    default:
        std::cout << "special key: 0x" << std::hex << chr << "\n";
    }
    if (!animate)
        glutPostRedisplay();
}


static void resetSim()
{
    std::cout << "starting seed: " << seed << "\n";
    bool enableVbo = pSim && pSim->isUsingVBO();
    bool enableCompute = pSim && pSim->isUsingGpuPhysics();
    if (pSim)
        delete pSim;
    pSim = new sim(seed, WORLD_SIDE, num_particles, (size_t)num_spheres);
    if (enableVbo) {
        pSim->toggleUseVBO();
    }
    if (enableCompute) {
        pSim->toggleUseGpuPhysics();
    }
}

static void updateVerbosity(int dv)
{
  if (verbosity == -1 && dv < 0) {
    std::cout << "verbosity is already at minimum\n";
    return;
  } else if (verbosity == 3 && dv > 0) {
    std::cout << "verbosity is already at maximum\n";
    return;
  }
  verbosity += dv;
  if (dv > 0 && verbosity == 1) {
    GL_CALL(glEnable, GL_DEBUG_OUTPUT_SYNCHRONOUS);
    GL_CALL(glDebugMessageCallback, openglDebugCallback, NULL);
    GL_CALL(glEnable, GL_DEBUG_OUTPUT);
  } else if (dv < 0 && verbosity == 0) {
    GL_CALL(glDisable, GL_DEBUG_OUTPUT_SYNCHRONOUS);
    GL_CALL(glDebugMessageCallback, NULL, NULL);
    GL_CALL(glDisable, GL_DEBUG_OUTPUT);
  }
}

static void changeObjects(int dobjs) {
  if (pSim) {
      num_spheres = std::max(0, num_spheres + dobjs);
      resetSim();
  }
}

static void keyPressed(unsigned char chr, int x, int y)
{
    switch (chr) {
    case '?':
    case 'h':
        std::cout <<
            "<mwheel>   zoom\n"
            "<arrows>   rotate camera\n"
            "<space>    one advection step\n"
            "<esc>      exit\n"
            "[,]        halve/double particles\n"
            "<,>        decrease/increase objects\n"
            "a          toggles animation (automatically advects)\n"
            "d          dump debug info\n"
            "g          toggle render VBO\n"
            "p          toggle physics compute shader\n"
            "r          reset\n"
            "R          randomizes the start seed\n"
            "v          verbose output\n"
            "w          toggle wireframes\n"
            "\n";
        break;
    ////////////////////////////////
    case 0x1B: // esc
        glutLeaveMainLoop();
        break;
    case ' ': // step one frame
        if (!animate)
            advect(MAX_DT);
        break;
    case ']':
        if (num_particles == 0)
            num_particles = 1;
        else
            num_particles *= 2;
        std::cout << "increasing particles to " << num_particles << "\n";
        resetSim();
        glutPostRedisplay();
        break;
    case '[':
        num_particles /= 2;
        std::cout << "decreasing particles to " << num_particles << "\n";
        resetSim();
        glutPostRedisplay();
        break;
    case '<':
      changeObjects(-1);
      break;
    case '>':
      changeObjects(1);
      break;
    case 'a': // animate
        animate = !animate;
        if (!animate)
            glutIdleFunc(nullptr);
        else
            glutIdleFunc(idle);
        break;
    case 'd': // debug
        std::cout << "x_rot: " << x_rot << "\n";
        std::cout << "y_rot: " << y_rot << "\n";
        if (pSim)
            pSim->debug(verbosity);
        break;
    case 'g':
        if (pSim) {
            pSim->toggleUseVBO();
            std::cout << "render VBO: " <<
                (pSim->isUsingVBO() ? "enabled" : "disabled") << "\n";
            glutPostRedisplay();
        }
        break;
    case 'p': // compute
        if (pSim) {
            pSim->toggleUseGpuPhysics();
            std::cout << "gpu physics: " <<
                (pSim->isUsingGpuPhysics() ? "enabled" : "disabled") << "\n";
            glutPostRedisplay();
        }
        break;
    case 'r': // reset
        resetSim();
        glutPostRedisplay();
        break;
    case 'R': // randomize start locations
        seed = std::random_device()();
        resetSim();
        glutPostRedisplay();
        break;
    case 'V':
        updateVerbosity(-1);
        break;
    case 'v':
        updateVerbosity(1);
        break;
    case 'w': // wireframes
        if (pSim) {
            pSim->toggleWireFrame();
            glutPostRedisplay();
        }
        break;
    default:
        std::cout << "0x" << std::hex << (int)chr << "\n";
    }
}

static void mouseWheel(int wheel, int direction, int x, int y)
{
    // std::cout << "wheel: " << wheel << "; dir: " << direction << "\n";
    if (direction > 0) {
        scale *= 1.1f;
    } else {
        scale *= 0.9f;
        if (scale < 0.1f) {
            scale = 0.1f;
        }
    }
}

static void mouseClick(int button, int state, int x, int y)
{
    if (button == GLUT_LEFT_BUTTON && state == GLUT_UP) {

    } else {
        std::cout << "mouseClick: button:" << button << ", state:" << state << "\n";
    }
}

static void reshape(int w, int h)
{
    std::cout << "reshape: " << w << " x " << h << "\n";
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, w/(float)h, 0.1, 100.0);
    glMatrixMode(GL_MODELVIEW);
}


int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(1024, 768);
    glutInitWindowPosition(10, 10);
    glutCreateWindow("physics");
    GLenum err = glewInit(); // must succeed glutCreateWindow
    if (GLEW_OK != err) {
        std::cerr << "glewInit: failed\n";
        return 1;
    }

    // clearGlErrors();
    checkGlErrors();

    // GL_CALL(glEnable, GL_DEBUG_OUTPUT_SYNCHRONOUS);
    // GL_CALL(glDebugMessageCallback, openglDebugCallback, NULL);
    // GL_CALL(glEnable, GL_DEBUG_OUTPUT);

    GL_CALL(glEnable, GL_LINE_SMOOTH);
    GL_CALL(glEnable, GL_POLYGON_SMOOTH);
    GL_CALL(glHint, GL_LINE_SMOOTH_HINT, GL_NICEST);
    GL_CALL(glHint, GL_POLYGON_SMOOTH_HINT, GL_NICEST);
    // GL_CALL(glEnable, GL_BLEND);
    // GL_CALL(glBlendFunc, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    GL_CALL(glClearColor, 0.0, 0.0, 0.0, 1.0);
    GL_CALL(glEnable, GL_DEPTH_TEST);
    // GL_CALL(glDepthFunc, GL_LEQUAL); // default is LESS
    GL_CALL(glEnable, GL_CULL_FACE);
    // GL_CALL(glClearDepth,1.0f);
    GL_CALL(glShadeModel, GL_SMOOTH);
    // GL_CALL(glEnable, GL_LIGHTING);
    GL_CALL(glEnable, GL_LIGHT0);
    static const GLfloat light0_pos[] = {1.0f, 10.0f, 3.0f, 1.0f};
    GL_CALL(glLightfv, GL_LIGHT0, GL_POSITION, light0_pos);
    static const GLfloat ambient0[] = {1.0f, 1.0f, 1.0f, 1.0f};
    GL_CALL(glLightfv, GL_LIGHT0, GL_AMBIENT, ambient0);
    static const GLfloat diffuse0[] = {1.0f, 1.0f, 1.0f, 1.0f};
    GL_CALL(glLightfv, GL_LIGHT0, GL_DIFFUSE, diffuse0);
    static const GLfloat specular0[] = {1.0f, 1.0f, 1.0f, 1.0f};
    GL_CALL(glLightfv, GL_LIGHT0, GL_SPECULAR, specular0);

    glutDisplayFunc(display);
    glutVisibilityFunc(visible);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyPressed);
    glutMouseFunc(mouseClick);
    glutMouseWheelFunc(mouseWheel);
    glutSpecialFunc(special);

    // glMatrixMode(GL_MODELVIEW);
    // std::cout << "MODEL VIEW\n";
    // std::cout << matrixString(GL_MODELVIEW_MATRIX);

    // std::cout << "MODEL VIEW (after glLookAt)\n";
    // std::cout << matrixString(GL_MODELVIEW_MATRIX);

    std::cout << "vendor: "   << glGetString(GL_VENDOR) << "\n";
    std::cout << "renderer: " << glGetString(GL_RENDERER) << "\n";
    std::cout << "version:  " << glGetString(GL_VERSION) << "\n";
    std::cout << "********************************************\n";

    resetSim();
    checkGlErrors();


    glutMainLoop();

    return 0;
}
