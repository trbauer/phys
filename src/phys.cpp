// #define GL_GLEXT_PROTOTYPES
// #include "GL/glcorearb.h"
// #include "GL/glext.h"
#include "GL/glew.h"
#include "GL/freeglut.h"


#include "gl.hpp"
#include "phys.hpp"

#include <algorithm>
#include <chrono>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <random>
#include <vector>


static int nextPowerOfTwo(int x) {
    x--;
    x |= x >> 1; // handle 2 bit numbers
    x |= x >> 2; // handle 4 bit numbers
    x |= x >> 4; // handle 8 bit numbers
    x |= x >> 8; // handle 16 bit numbers
    x |= x >> 16; // handle 32 bit numbers
    x++;
    return x;
}

sim::sim(size_t seed, float w, size_t _num_particles, size_t _num_spheres)
    : num_particles(nextPowerOfTwo(_num_particles))
    , min_pos(-w,-w,-w)
    , max_pos( w, w, w)
{
    gen.seed(seed);

    std::uniform_real_distribution<float> init_pos(-w, w);
    std::uniform_real_distribution<float> init_vel(-w/10.0f, w/10.0f);

    // randomize the spheres
    spheres.resize(_num_spheres);
    std::uniform_real_distribution<float> init_rad_big(1.0f,3.0f);
    std::uniform_real_distribution<float> init_rad_small(0.5f,1.0f);
    for (size_t i = 0; i < spheres.size(); i++) {
        sphere &s = spheres[i];
        s.pos.x = init_pos(gen);
        s.pos.y = i < 1 ? -WORLD_SIDE : init_pos(gen);
        s.pos.z = init_pos(gen);
        s.rad = i < 1 ? init_rad_big(gen) : init_rad_small(gen);
    }

    // randomize the particles second
    gen.seed(seed * seed);
    particles = new bufobj<particle>(
        GL_ARRAY_BUFFER, // GL_SHADER_STORAGE_BUFFER,
        num_particles,
        GL_STATIC_DRAW);
    particles->modify(
        [&] (particle &p) {
            memset(&p, 0, sizeof(particle));

            p.pos.x = 0.9f*init_pos(gen);
            p.pos.y = fabs(init_pos(gen));
            p.pos.z = 0.9f*init_pos(gen);

            p.vel.x = init_vel(gen);
            p.vel.y = init_vel(gen);
            p.vel.z = init_vel(gen);

            p.live = true;
            // p.vel = glm::vec3(0.0f,0.0f,0.0f);
            p.pos0 = p.pos;
            p.vel0 = p.vel;
        });
}

sim::~sim()
{
    if (particles) {
        delete particles;
        particles = nullptr;
    }
    if (compute_shader) {
        delete compute_shader;
        compute_shader = nullptr;
    }
}

void sim::toggleUseGpuPhysics()
{
    if (compute_shader) {
        delete compute_shader;
        compute_shader = nullptr;
    } else {
        try {
            std::map<std::string,std::string> pp_vars;
            pp_vars["SPHERE_COUNT"] = std::to_string(spheres.size());
            compute_shader = new prog("phys.glsl", GL_COMPUTE_SHADER, pp_vars);
            auto getUniformLoc = [&](const char *symbol) {
                auto x = glGetUniformLocation(
                    compute_shader->getProgramId(),
                    symbol);
                checkGlErrors("glGetUniformLocation");
                return x;
            };
            dt_loc = getUniformLoc("dt");
            w_loc = getUniformLoc("W");
            spherePos_loc = getUniformLoc("spherePos");
            sphereRad_loc = getUniformLoc("sphereRad");

            auto bindBuffer = [&](
                const char *bufferName,
                GLuint ssboBindingPoint,
                GLuint bufferId)
            {
                GLuint block_index =
                    glGetProgramResourceIndex(
                        compute_shader->getProgramId(),
                        GL_SHADER_STORAGE_BLOCK,
                        bufferName);
                checkGlErrors("glGetProgramResourceIndex");
                GL_CALL(glShaderStorageBlockBinding,
                    compute_shader->getProgramId(),
                    block_index,
                    ssboBindingPoint);
                GL_CALL(glBindBufferBase,
                    GL_SHADER_STORAGE_BUFFER,
                    ssboBindingPoint,
                    bufferId);
            };

            bindBuffer("particles", 2, particles->bufferId());
            // bindBuffer("spheres", 3, static_geometry->bufferId());
        } catch (const prog_error &pe) {
            std::cerr << pe.error << "\n";
        }
    }
}

void sim::debug(int verbosity)
{
    std::cout << "**** OBJECTS *****\n";
    for (const sphere &s : spheres) {
        std::cout << "  sphere " << s.rad << "  " << format(s.pos) << "\n";
    }
    std::cout << "**** PARTICLES *****\n";
    particles->read([&] (const particle *ps, size_t ps_len) {
        const int CUTOFF = verbosity < 2 ? 4 : ps_len;
        for (size_t i = 0; i < ps_len && i < CUTOFF; i++) {
            std::cout << "  particle " <<
                format(ps[i].pos) << "  " << format(ps[i].vel);
            if (ps[i].live) {
                std::cout << ", live";
            } else {
                std::cout << ", dead";
            }
            if (ps[i].pinned) {
                std::cout << ", pinned";
            }
            std::cout << "\n";
        }
        if (ps_len > CUTOFF) {
            std::cout << "... " << std::dec << ps_len - CUTOFF << " more\n";
        }
    });

//    buffer_map<particle> bm = particles.map(GL_READ_ONLY);
/*
    particles->modify(
        [&] (particle *ps, size_t psLen) {
            for (size_t i = 0; i < psLen; i++) {
                particle &p = ps[i];
                const sphere &s = spheres[0];
                if (glm::distance2(p.pos,s.pos) < s.rad*s.rad) {
                    std::cout <<  "found bad particle " << format(p.pos) << "\n";
                    std::cout << "p.pos = glm::vec3(" <<
                        p.pos0.x << ", " << p.pos0.y << ", " << p.pos0.z << ");\n";
                    std::cout << "p.vel = glm::vec3(" <<
                        p.vel0.x << ", " << p.vel0.y << ", " << p.vel0.z << ");\n";
                    // break;
                    p.pinned = true;
                } else {
                    p.pinned = false;
                }
            }
        });
*/
}

void sim::advect(float dt)
{
    if (compute_shader) {
        advectHw(dt);
    } else {
        advectSw(dt);
    }
}

void sim::render()
{
    if (renderWireframes) {
        glCullFace(GL_FRONT_AND_BACK);
    } else {
        glCullFace(GL_BACK);
    }

    // always render the cube in SW
    glColor3f(1,1,1);
    glutWireCube(max_pos.x - min_pos.x);

    glColor3f(0.4f,0.3f,0.5f);
    const int STRIPS = 4;
    const float dx = (max_pos.x - min_pos.x)/STRIPS;
    const float dz = (max_pos.z - min_pos.z)/STRIPS;
    // X-major, Z-minor (strips are on z axes)
    // STRIP #1
    //  1  3  5  7
    //  2  4  6  8
    // STRIP #2 (+=z)
    //  9 11 13 15
    // 10 12 14 16
    for (int z_strip = 0; z_strip < STRIPS; z_strip++) {
        glBegin(GL_TRIANGLE_STRIP);
        for (int x_strip = 0; x_strip <= STRIPS; x_strip++) {
            glVertex3f(
                min_pos.x + x_strip*dx,
                min_pos.y - EPSILON,
                min_pos.z + z_strip*dz);
            glVertex3f(
                min_pos.x + x_strip*dx,
                min_pos.y - EPSILON,
                min_pos.z + (z_strip + 1)*dz);
        }
        glEnd();
    }

    // render the particles
    if (useRenderVBO) {
        // https://stackoverflow.com/questions/12742142/opengl-compute-shader-invocations
//        GL_CALL(glMemoryBarrier, GL_VERTEX_ATTRIB_ARRAY_BARRIER_BIT);
        renderHw();
    } else {
        // See above.  This assumes the buffer map op needs a barrier.
        // TODO: determine if needed
//        GL_CALL(glMemoryBarrier, GL_SHADER_STORAGE_BARRIER_BIT);
        renderSw();
    }

    // for now we have to render the static geometry in SW
    // (need to generate a vertex array for the sphere)
    for (const sphere &s : spheres) {
        // http://goanna.cs.rmit.edu.au/~gl/teaching/Interactive3D/2015/hierarchical-modelling.html
        glPushMatrix();
        glColor3f(0.5f,0.4f,0.2f);
        glTranslatef(s.pos.x,s.pos.y,s.pos.z);
        glScalef(s.rad,s.rad,s.rad);
        // glRotatef(y_rot,0,1,0);
        int ss = 32;
        if (renderWireframes) {
            glutWireSphere(1.0,ss,ss);
        } else {
            glutSolidSphere(1.0,ss,ss);
        }
        glPopMatrix();
    }
}

void sim::renderSw()
{
    // render the vertices
    glPointSize(2.0f);
    glColor3f(1,1,1);

    particles->read([&](const particle *ps, size_t len) {
        glBegin(GL_POINTS);
        for (size_t i = 0; i < len; i++) {
            if (!ps[i].live) {
                glColor3f(1.0f,0.0f,0.0f);
            } else if (ps[i].pinned) {
                glColor3f(1.0f,1.0f,0.0f);
            } else {
                glColor3f(1.0f,1.0f,1.0f);
            }
            glVertex3fv(&ps[i].pos.x);
        }
        glEnd();
    });
}

void sim::renderHw()
{
    glPointSize(2.0f);
    glColor3f(1,1,1);

    // VBO example http://www.songho.ca/opengl/gl_vbo.html
    GL_CALL(glBindBuffer, GL_ARRAY_BUFFER, particles->bufferId());
    GL_CALL(glNormalPointer, GL_FLOAT, 0, nullptr);
    GL_CALL(glVertexPointer, 3, GL_FLOAT, sizeof(particle), nullptr);
    // GL_CALL(glBindBuffer, GL_ELEMENT_ARRAY_BUFFER, particle_indices->bufferId());
    // GL_CALL(glIndexPointer, GL_UNSIGNED_SHORT, 0, 0);

    GL_CALL(glEnableClientState, GL_VERTEX_ARRAY);
    GL_CALL(glDrawArrays, GL_POINTS, 0, particles->size());
    GL_CALL(glDisableClientState, GL_VERTEX_ARRAY);

    GL_CALL(glBindBuffer, GL_ARRAY_BUFFER, 0);
    // GL_CALL(glBindBuffer, GL_ELEMENT_ARRAY_BUFFER, 0);
}

static inline glm::vec3 collideWith(glm::vec3 v, glm::vec3 n)
{
    // r_v = v - 2*(v `dot` n)*n
    // return v - 2.0f * glm::dot(v,n) * n;
    return 0.8f * glm::reflect(v, n);
}

void sim::advectParticleSw(
    particle &p,
    size_t p_index,
    float dt,
    const sphere *os,
    size_t os_len)
{
    if (!p.live)
        return;

    // std::cout << "particle\n";
    // std::cout << "  pos: " << format(p.pos) << "\n";
    // std::cout << "  vel: " << format(p.vel) << "\n";
    glm::vec3 pos0 = p.pos;
    p.pos += dt*p.vel;
    p.vel.y += -9.8f*dt;
    p.vel.y = glm::clamp(p.vel.y,-MAX_VEL,MAX_VEL);

    // check for collision with floor
    if (p.pos.y < min_pos.y) {
        // std::cout << "collided with floor\n";
        p.pos.y = min_pos.y;
        p.vel = collideWith(p.vel,glm::vec3(0,1,0));
        p.vel *= 0.2f; // dampen with hit on floor
        return;
    }
    // max-x wall
    if (p.pos.x > max_pos.x) {
        p.pos.x = max_pos.x;
        p.vel = collideWith(p.vel,glm::vec3(-1,0,0));
        return;
    }
    // min-x wall
    if (p.pos.x < min_pos.x) {
        p.pos.x = min_pos.x;
        p.vel = collideWith(p.vel,glm::vec3(1,0,0));
        return;
    }
    // max-z wall
    if (p.pos.z > max_pos.z) {
        p.pos.z = max_pos.z;
        p.vel = collideWith(p.vel,glm::vec3(0,0,-1));
        return;
    }
    // min-z wall
    if (p.pos.z < min_pos.z) {
        p.pos.z = min_pos.z;
        p.vel = collideWith(p.vel,glm::vec3(0,0,1));
        return;
    }

    // collision with a sphere
    for (size_t sIx = 0; sIx < os_len; sIx++) {
        const sphere &s = os[sIx];
        auto i = p.pos - s.pos; // incident
                                // a dot b = |a||b|cos(angle_between(a,b))
        bool exiting_sphere = dot(i,p.vel) > 0.0f;
        //            std::cout << "  dot: " << dot(n,p.vel) << "\n";
        if (!exiting_sphere && dot(i,i) < s.rad*s.rad) {
            // std::cout << "collided with sphere\n";
            p.pos = s.pos + i; // stop at collision point
            p.vel = collideWith(p.vel, glm::normalize(i));
            break;
        }
    }
}

void sim::advectSw(float dt)
{
    // check for collision with any of the spheres
    particles->modify([&](particle &p, size_t index) {
        advectParticleSw(p, index, dt, spheres.data(), spheres.size());
    });
}

void sim::advectHw(float dt)
{
    GL_CALL(glMemoryBarrier, GL_SHADER_STORAGE_BARRIER_BIT);

    GL_CALL(glUseProgram, compute_shader->getProgramId());
    int num_workgroups = num_particles / 32;
    if (num_particles % 32) {
        num_workgroups += 1;
    }
    GL_CALL(glUniform1f, dt_loc, dt);
    GL_CALL(glUniform1f, w_loc, max_pos.x);
    std::vector<glm::vec3> sps;
    std::vector<GLfloat> srs;
    for (const sphere &s : spheres) {
        sps.push_back(s.pos);
        srs.push_back(s.rad);
    }
    GL_CALL(glUniform3fv, spherePos_loc, sps.size(), &sps[0].x);
    GL_CALL(glUniform1fv, sphereRad_loc, srs.size(), srs.data());

    GL_CALL(glBindBufferBase,
        GL_SHADER_STORAGE_BUFFER,
        2, // binding=2 in phys.gl
        particles->bufferId());
    GL_CALL(glDispatchCompute, num_workgroups, 1, 1);

    GL_CALL(glMemoryBarrier, GL_ALL_BARRIER_BITS); // TODO: remove
    GL_CALL(glMemoryBarrier, GL_SHADER_STORAGE_BARRIER_BIT); // TODO: remove
    GL_CALL(glUseProgram, 0);
    // glFlush();
    // glFinish();
}
