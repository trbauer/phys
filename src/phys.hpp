#ifndef PHYS_HPP
#define PHYS_HPP

#include "geom.hpp"
#include "prog.hpp"
#include "gl.hpp"
#include "bufobj.hpp"


#include <algorithm>
#include <vector>
#include <random>
#include <iostream>

static const float WORLD_SIDE = 5.0f;
static const float MAX_VEL = 32.0f;
static const float MAX_DT = 0.02f;
static const float EPSILON = 0.0001f;

struct particle {
    // 32 bytes
    glm::vec3 pos; float _padding0;
    glm::vec3 vel; float _padding1;

    // 32 bytes
    glm::vec3 pos0; float _padding2;
    glm::vec3 vel0; bool pinned, live, _padding3[2];

    particle()
        : pinned(false)
        , live(false)
    {
    }
}; // particle
static_assert(sizeof(particle) == 64, "sizeof(particle)");
struct sphere {
    glm::vec3   pos;
    float       rad;

    sphere() { }
    sphere(float r) : rad(r) { }
};
static_assert(sizeof(sphere) == 16, "sizeof(sphere)");

struct plane {
    glm::vec3   norm;  float _padding0;
    glm::vec3   pos;   float _padding1;
};
static_assert(sizeof(plane) == 32, "sizeof(plane)");

struct triangle {
    glm::vec3   v0, v1, v2;
    glm::vec3   n;
};
static_assert(sizeof(triangle) == 48, "sizeof(triangle)");

// enum Material{PLASTIC, WOOD, METAL, ...};
// struct shape
//    enum{SPHERE,BOX,TRI} type;
//    // sphere
//    float rad; // valid for all for a cheap first check....
//    // box
//    float4 dims; // w/h/d (x,y,z)
//    float3 vs[3];
//    // box and tri
//    float4 orient; // orientation (quaternion)
//
//    // material, etc...
//    Material material;
// };

class sim {
    std::mt19937            gen;

    glm::vec3               min_pos, max_pos;
    size_t                  num_particles;

    // non-nullptr implies we are using GPU physics
    prog                   *compute_shader = nullptr;

    GLint                   dt_loc = -1, w_loc = -1;
    GLint                   spherePos_loc = -1, sphereRad_loc = -1;

    // TODO: lift to scalar
    bufobj<particle>       *particles = nullptr;
    // bufobj<sphere>         *static_geometry = nullptr;
    std::vector<sphere>     spheres;

    bool                    useRenderVBO = false;
    bool                    renderWireframes = false;

  public:
    sim(size_t seed, float w, size_t num_particles, size_t numSpheres);
    ~sim();

    size_t numParticles() const {return num_particles;}
    size_t numSpheres() const {return spheres.size();}

    void toggleUseGpuPhysics();
    bool isUsingGpuPhysics() const {return compute_shader != nullptr;}

    void toggleUseVBO() {useRenderVBO = !useRenderVBO;}
    bool isUsingVBO() {return useRenderVBO;}

    void toggleWireFrame() {renderWireframes = !renderWireframes;}

    void debug(int verbosity);

    void advect(float dt);
    void render();
private:
    void renderSw();
    void renderHw();
    void advectSw(float dt);
    void advectParticleSw(
            particle &p,
            size_t p_index,
            float dt,
            const sphere *os,
            size_t os_len);
    void advectHw(float dt);
};

#endif