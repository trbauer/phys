#include "prog.hpp"
#include "gl.hpp"

#include <fstream>
#include <functional>
#include <iomanip>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>


static std::string getShaderOrProgramLog(GLuint prog_or_shader_id)
{
    auto is_prog = glIsProgram(prog_or_shader_id);
    checkGlErrors("glIsProgram");
    GLint info_log_len = 0;
    if (is_prog) {
        GL_CALL(glGetProgramiv, prog_or_shader_id, GL_INFO_LOG_LENGTH, &info_log_len);
    } else {
        GL_CALL(glGetShaderiv, prog_or_shader_id, GL_INFO_LOG_LENGTH, &info_log_len);
    }

    GLchar *info_log = new GLchar[info_log_len + 1];
    if (is_prog) {
        GL_CALL(glGetProgramInfoLog, prog_or_shader_id, info_log_len, nullptr, info_log);
    } else {
        GL_CALL(glGetShaderInfoLog, prog_or_shader_id, info_log_len, nullptr, info_log);
    }
    info_log[info_log_len] = 0;
    std::string s = std::string(info_log);
    delete[] info_log;

    return s;
}
// void glGetShaderInfoLog(	GLuint shader,
//    GLsizei maxLength,
//    GLsizei *length,
//    GLchar *infoLog);
// void glGetProgramInfoLog(	GLuint program,
//    GLsizei maxLength,
//    GLsizei *length,
//    GLchar *infoLog);
//
// GLEW screws this approach up
//
// typedef void (*GLGetLogFunction)(GLuint,GLsizei,GLsizei*,GLchar*);

prog::prog(
    const char *filename,
    GLenum shader_type,
    std::map<std::string,std::string> pp_vars)
{
    auto throwError =
        [&] (std::string str) {
            if (shader_id) {
                glDeleteShader(shader_id);
                shader_id = 0;
            }
            if (program_id) {
                glDeleteProgram(program_id);
                program_id = 0;
            }
            std::stringstream ss;
            ss << "loading " << filename << ": " << str;
            throw prog_error(ss.str());
        };
    program_id = glCreateProgram();
    checkGlErrors("glCreateProgram");
    GL_CALL(glProgramParameteri, program_id, GL_PROGRAM_SEPARABLE, GL_TRUE);
    GL_CALL(glProgramParameteri, program_id, GL_PROGRAM_BINARY_RETRIEVABLE_HINT, GL_TRUE);

    shader_id = glCreateShader(shader_type);
    checkGlErrors("glCreateShader");

    // Search the current working directory first, then Release/ and debug
    // after that.
    // - The former are the expected sensible location.
    // - The latter will capture where cmake copies of file to to ease MSVS
    //   debugging.
    std::ifstream in(filename, std::ios::in | std::ios::binary);
    if (!in) {
        std::string fn2 = std::string("Release/") + filename;
        in.open(fn2, std::ios::in | std::ios::binary);
    }
    if (!in) {
        std::string fn2 = std::string("Debug/") + filename;
        in.open(fn2, std::ios::in | std::ios::binary);
    }
    if (!in) {
        throwError("could not open file");
    }
    in.seekg(0, std::ios::end);
    size_t flen = (size_t)in.tellg();
    if (flen == (size_t)-1) {
        throwError("file read error");
    }
    std::string src;
    src.resize(static_cast<unsigned int>(flen));
    in.seekg(0, std::ios::beg);
    in.read(&src[0], src.size());
    in.close();

    std::stringstream ss;
    int lno = 1;
    for (size_t i = 0, slen = src.size(); i < slen; i++) {
        if (src[i] == '\n') {
            lno++;
            ss << '\n';
            continue;
        }

        // expand ${VAR} to the value given in the map
        if (i < src.size() - 1 && src[i] == '$') {
            if (src[i + 1] != '{') {
              throwError(std::to_string(lno) + ". preprocessor missing {");
            }
            std::string var;
            for (size_t k = i + 2; k < slen; k++) {
                if (src[k] == '}') {
                    var = src.substr(i + 2, k - i - 2);
                    i = k;
                    break;
                }
            }
            if (var.empty())
                throwError(std::to_string(lno) + ". preprocessor token missing }");
            auto it = pp_vars.find(var);
            if (it == pp_vars.end()) {
                throwError(
                    std::to_string(lno) + ". preprocessor variable "
                    "${" + var + "} lacks value");
            }
            ss << it->second;
        } else {
            ss << src[i];
        }
    }
    source = ss.str();

    const char *src_c = source.c_str();
    GL_CALL(glShaderSource, shader_id, 1, &src_c, NULL);
    GL_CALL(glCompileShader, shader_id);
    GLint compile_status;
    GL_CALL(glGetShaderiv, shader_id, GL_COMPILE_STATUS, &compile_status);
    GLint err = 0;
    if (compile_status == GL_FALSE) {
        std::stringstream ss;
        if (err != 0) {
            ss << formatGlError(err) << " (" << err << ")";
        }
        ss << "\n";
        ss << getShaderOrProgramLog(shader_id);
        throwError(ss.str());
    }

    GL_CALL(glAttachShader, program_id, shader_id);
    GL_CALL(glLinkProgram, program_id);
    GLint link_status;
    GL_CALL(glGetProgramiv, program_id, GL_LINK_STATUS, &link_status);
    if (link_status == GL_FALSE) {
        std::stringstream ss;
        if (err != 0) {
            ss << formatGlError(err) << " (" << err << ")";
        }
        ss << "\n";
        ss << getShaderOrProgramLog(program_id);
        throwError(ss.str());
    }

    // I don't think this is needed
    // GL_CALL(glDetachShader, program_id, shader_id);

    GLsizei bits_len;
    GL_CALL(glGetProgramiv, program_id, GL_PROGRAM_BINARY_LENGTH, &bits_len);
    std::vector<unsigned char> bits;
    bits.resize(bits_len);
    GLenum fmt = 0;
    GL_CALL(glGetProgramBinary,
        program_id,
        (GLsizei)bits.size(),
        nullptr,
        &fmt,
        bits.data());

    std::ofstream of("prog.bits", std::ios::binary);
    of.write((const char *)bits.data(), bits.size());
}

prog::~prog()
{
    if (shader_id) {
        glDeleteShader(shader_id);
        shader_id = 0;
    }
    if (program_id) {
        glDeleteProgram(program_id);
        program_id = 0;
    }
}
