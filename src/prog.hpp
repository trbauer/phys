#ifndef PROG_HPP
#define PROG_HPP

#include "GL/glew.h"

#include <exception>
#include <stdexcept>
#include <map>

struct prog_error : std::runtime_error {
    std::string error;
    prog_error(std::string s) : std::runtime_error(s), error(s) { }
};

class prog
{
    GLuint        program_id = 0;
    GLuint        shader_id = 0;
    std::string   source;
public:
    // GL_COMPUTE_SHADER
    prog(const char *file, GLenum type, std::map<std::string,std::string> pp_vars);
    ~prog();

    GLuint getProgramId() const { return program_id; }
    GLuint getShaderId() const { return shader_id; }
};


#endif